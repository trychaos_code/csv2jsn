var prts = require("./port.json");

var parsedDta = {};

Object.keys(prts).forEach(i=>{
    Object.keys(prts[i]).forEach(i1=>{
        prts[i][i1].forEach(p=>{
            if(i!== 'default' && (~i1.indexOf('smtp') || i1==='mail' || i1==='outgoing')){
                let ot = {
                    host: i1+'.'+i,
                    port: p,
                    secure: true
                };
                parsedDta[i] || (parsedDta[i] = {});
                parsedDta[i].ot || (parsedDta[i].ot = []);
                parsedDta[i].ot.push(ot);
            }else if(i!== 'default' && (~i1.indexOf('imap') || i1==='outlook' || i1==='incoming')){
                let inc = {
                    host: i1+'.'+i, // smtp.mail.yahoo
                    port: p,
                    tls: true,
                    secure: false,
                    authTimeout: 5000,
                };
                parsedDta[i] || (parsedDta[i] = {});
                parsedDta[i].inc || (parsedDta[i].inc = []);
                parsedDta[i].inc.push(inc);
            }
        })
    });
    if (i==='default'){
        prts[i]['smtp-key'].forEach(k=>{
            prts[i]['smtp-value'].forEach(k1=>{
                let ot = {
                    host: k,
                    port: k1,
                    secure: true
                };
                parsedDta[i] || (parsedDta[i] = {});
                parsedDta[i].ot || (parsedDta[i].ot = []);
                parsedDta[i].ot.push(ot);
            })
        });
        prts[i]['imap-key'].forEach(k=>{
            prts[i]['imap-value'].forEach(k1=>{
                let inc = {
                    host: k,
                    port: k1,
                    secure: true
                };
                parsedDta[i] || (parsedDta[i] = {});
                parsedDta[i].inc || (parsedDta[i].inc = []);
                parsedDta[i].inc.push(inc);
            })
        });
    }
});

console.log(JSON.stringify(parsedDta,null,1));
